#include <stdio.h>
#include <stdlib.h>
#include "stack.h"


int main() {

  printf("Stack Test\n\n");

  stack *stack1 = create(5);
  int full, empty;
  full = stackFull(stack1);
  empty = stackEmpty(stack1);
  printf("isFull: %d", full);
  printf("\nisEmpty: %d", empty);
  push(&stack1,1);
  push(&stack1,3);
  push(&stack1,5);
  display(stack1);

  /*

  printf("Queue Test");

   int h, t, f, e;
   queue *q1 =  create(5);
   enqueue(&q1, 1);
   enqueue(&q1, 2);
   enqueue(&q1, 3);
   enqueue(&q1, 4);
   h = head(q1);
   t = tail(q1);
   f = queueFull(q1);
   e = queueEmpty(q1);
   display(q1);
   printf("\nHead: %d", h);
   printf("\nTail: %d", t);
   printf("\nisFull: %d", f);
   printf("\nisEmpty: %d\n\n", e);
   dequeue(&q1);
   dequeue(&q1);
   enqueue(&q1, 5);
   h = head(q1);
   t = tail(q1);
   f = queueFull(q1);
   e = queueEmpty(q1);
   display(q1);
   printf("\nHead: %d", h);
   printf("\nTail: %d", t);
   printf("\nisFull: %d", f);
   printf("\nisEmpty: %d\n\n", e);
   dequeue(&q1);
   dequeue(&q1);
   enqueue(&q1, 6);
   enqueue(&q1, 7);
   enqueue(&q1, 8);
   h = head(q1);
   t = tail(q1);
   f = queueFull(q1);
   e = queueEmpty(q1);
   display(q1);
   printf("\nHead: %d", h);
   printf("\nTail: %d", t);
   printf("\nisFull: %d", f);
   printf("\nisEmpty: %d\n\n", e);
   dequeue(&q1);
   dequeue(&q1);
   dequeue(&q1);
   dequeue(&q1);
   h = head(q1);
   t = tail(q1);
   f = queueFull(q1);
   e = queueEmpty(q1);
   display(q1);
   printf("\nHead: %d", h);
   printf("\nTail: %d", t);
   printf("\nisFull: %d", f);
   printf("\nisEmpty: %d\n\n", e);

   */

  return 0;
}
