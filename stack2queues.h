/*******************************************************************
YOU ARE NOT ALLOWED TO MODIFY THE STRUCT AND THE FUNCTION PROTOTYPES
*******************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include "queue.h"

typedef struct {
	int n;
	queue *queue1;
	queue *queue2;
} stack2queues;

stack2queues* createStack(int n) {
	// Your code here
}

int stackEmpty(stack2queues *s) {
	// Your code here
}

int stackFull(stack2queues *s) {
	// Your code here
}

void push(stack2queues **s, int data) {
	// Your code here
}

int pop(stack2queues **s) {
	// Your code here
}

void displayStack(stack2queues *s) {
	// Your code here
}

int top(stack2queues *s) {
	// Your code here
}
