/*******************************************************************
YOU ARE NOT ALLOWED TO MODIFY THE STRUCT AND THE FUNCTION PROTOTYPES
*******************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include "stack.h"

typedef struct {
	int n;
	stack *stack1;
	stack *stack2;
} queue2stacks;

queue2stacks* createQueue(int n) {
	// Your code here
}

int queueEmpty(queue2stacks *q) {
	// Your code here
}

int queueFull(queue2stacks *q) {
	// Your code here
}

void enqueue(queue2stacks **q, int data) {
	// Your code here
}

int dequeue(queue2stacks **q) {
    // Your code here
}

void displayQueue(queue2stacks *q) {
	// Your code here
}

int head(queue2stacks *q) {
	// Your code here
}

int tail(queue2stacks *q) {
	// Your code here
}

